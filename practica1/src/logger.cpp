#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



int main(int argc, char* argv[]) 
{
	mkfifo("/tmp/loggerfifo",0777);
	int fd=open("/tmp/loggerfifo", O_RDONLY);
	char buff[200]="Comienza el juego\n";
	int aux;	

	do
	{
		read(fd,buff,sizeof(buff));
		printf("%s\n", buff);
	}
	while(buff[0]!='C');

 	close(fd);
	unlink("/tmp/loggerfifo");
	return 0;
}
