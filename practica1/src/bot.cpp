#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()
{
	int file;
	DatosMemCompartida* pMemComp;
	char* proyeccion;

	file=open("/tmp/datosBot.txt",O_RDWR);

	proyeccion=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);

	close(file);

	pMemComp=(DatosMemCompartida*)proyeccion;

	while(1)
	{
		float posRaqueta1;
		float posRaqueta2;
		posRaqueta1=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		posRaqueta2=((pMemComp->raqueta2.y2+pMemComp->raqueta2.y1)/2);
		if(posRaqueta1<pMemComp->esfera.centro.y)
			pMemComp->accion1=1;
		else if(posRaqueta1>pMemComp->esfera.centro.y)
			pMemComp->accion1=-1;
		else
			pMemComp->accion1=0;
		if(posRaqueta2<pMemComp->esfera.centro.y)
			pMemComp->accion2=1;
		else if(posRaqueta2>pMemComp->esfera.centro.y)
			pMemComp->accion2=-1;
		else
			pMemComp->accion2=0;
		usleep(25000);  
	}

	munmap(proyeccion,sizeof(*(pMemComp)));
}
